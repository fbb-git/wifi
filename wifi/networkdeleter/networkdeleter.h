#ifndef INCLUDED_NETWORKDELETER_
#define INCLUDED_NETWORKDELETER_


#include <string>
#include <vector>

#include <bobcat/pattern>

class NetworkDeleter
{
    struct Entry
    {
        size_t      d_begin;    // index in d_file of begin of network spec.
        size_t      d_end;      // index in d_file beyond network spec.
        std::string d_ssid;
        bool        d_delete = false;
    };

    struct EntryInfo
    {
        bool d_sawEnd;      // network's ending curly brace has been seen
        Entry d_entry;
    };

    std::vector<std::string> d_file;
    std::string d_wpa_conf;
    std::vector<Entry> d_entry;

    static FBB::Pattern s_network;
    static FBB::Pattern s_ssid;
    static FBB::Pattern s_curlyClose;
    static FBB::Pattern s_blank;

    public:
        NetworkDeleter();
        ~NetworkDeleter();
        void optDeleteNetworks();
        

    private:
        void add(EntryInfo &entry, std::string const &line);
        bool setBeginEnd(size_t *begin, size_t *end) const;
        void queryDeletes(size_t begin, size_t end);
        void tryPush_back(EntryInfo &entry);
};
        
#endif






