#include "networkdeleter.ih"

//                    ssid="styx"
Pattern NetworkDeleter::s_ssid("^\\s+ssid=\"([[:print:]]+)\"");

//                    network={
Pattern NetworkDeleter::s_network("^\\s*network\\s*=");

//                    }
Pattern NetworkDeleter::s_curlyClose("^\\s*}\\s*(#.*)?$");

//                    blanks only
Pattern NetworkDeleter::s_blank("^\\s*$");
