#include "networkdeleter.ih"

void NetworkDeleter::tryPush_back(EntryInfo &entry)
{
    if (entry.d_sawEnd)
    {
        ++entry.d_entry.d_end;
        d_entry.push_back(entry.d_entry);
    }
}
