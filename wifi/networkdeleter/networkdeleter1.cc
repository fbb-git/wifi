#include "networkdeleter.ih"

NetworkDeleter::NetworkDeleter()
{
    ArgConfig::instance().option(&d_wpa_conf, 'w');

    ifstream in;
    Exception::open(in, d_wpa_conf);

    EntryInfo entry {false};

    for (auto const &line: 
            ranger(istream_iterator<StringLine>(in), 
                   istream_iterator<StringLine>()))
        add(entry, line);

    tryPush_back(entry);
}
