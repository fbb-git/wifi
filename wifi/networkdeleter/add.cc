#include "networkdeleter.ih"

void NetworkDeleter::add(EntryInfo &entry, string const &line)
{
    size_t last = d_file.size();    // index of line, added next:

    d_file.push_back(line);

    if (entry.d_sawEnd)
    {
        if (s_blank << line)        // curlyClose was seen, add blank line
        {
            ++entry.d_entry.d_end;
            return;
        }

        tryPush_back(entry);
        entry.d_sawEnd = false;
    }
            
    if (s_network << line)                  // saw network= line: new network
        entry.d_entry.d_begin = last;
    else if (s_ssid << line)                // saw ssid specification
        entry.d_entry.d_ssid = s_ssid[1];
    else if (s_curlyClose << line)            // saw curly end
    {
        entry.d_entry.d_end = last;
        entry.d_sawEnd = true;
    }
}



