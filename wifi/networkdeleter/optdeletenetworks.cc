#include "networkdeleter.ih"

void NetworkDeleter::optDeleteNetworks()
{
    cout << '\n';

    if (d_entry.empty())
    {
        cout << d_wpa_conf << " does not contain network specifications\n";
        return;
    }

    size_t begin;
    size_t end = 0;

    while (setBeginEnd(&begin, &end))
    {
        cout << "Networks " << (begin + 1) << " until " << end << 
                " of " << d_entry.size() << '\n';

        for (size_t next = begin; next != end; ++next)
            cout << "    " << setw(2) << (next + 1) << ": " << 
                    d_entry[next].d_ssid << '\n';

        queryDeletes(begin, end);
    }
}




