#include "networkdeleter.ih"

bool NetworkDeleter::setBeginEnd(size_t *begin, size_t *end) const
{
    if (*end == d_entry.size())     // reached the end
        return false;

    *begin = *end;

    size_t remaining = d_entry.size() - *begin;

    *end = 
        remaining <= 10 ? d_entry.size() :          // max 10 left: show all
        remaining <= 14 ? *begin + remaining / 2  : // max 14 left: show 1/2
                          *begin + 10;              // else show next 10

    return true;
}
