#include "networkdeleter.ih"

NetworkDeleter::~NetworkDeleter()
{
    ofstream out(d_wpa_conf);
    auto begin = d_file.begin();

    for (auto const &network: d_entry)
    {
        if (network.d_delete)
        {
            copy(begin, d_file.begin() + network.d_begin,
                ostream_iterator<string>(out, "\n"));
            begin = d_file.begin() + network.d_end;
        }
    }
    copy(begin, d_file.end(),
         ostream_iterator<string>(out, "\n"));
}
