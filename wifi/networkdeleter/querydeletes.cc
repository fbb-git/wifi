#include "networkdeleter.ih"

void NetworkDeleter::queryDeletes(size_t begin, size_t end)
{
    cout << 
        "Enter the number(s) of the networks to delete. Press `Enter' "
                                                                "to keep\n"
        "networks " << (begin + 1) << " until " << end << ": ";
    
    string line;
    getline(cin, line);

    size_t nr;
    istringstream ins(line);

    while (ins >> nr)
    {
        ins.ignore();       // skip a separator
        if (begin < nr && nr <= end)
        {
            d_entry[nr - 1].d_delete = true;
            cout << "    deleting network #" << nr << " (" << 
                    d_entry[nr -1].d_ssid << ")\n";
        }
    }
    cout << '\n';
}
