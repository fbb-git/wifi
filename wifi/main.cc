#include "main.ih"

namespace 
{
    ArgConfig::LongOption longOptions[] =
    {
        {"debug",                   'd'},
        {"interface",               'i'},
        {"wpa_conf",                'w'},

        {"usage",                   'h'},
        {"version",                 'v'},
        {"verbose",                 'V'},

    };

    auto longEnd = longOptions +
                        sizeof(longOptions) / sizeof(ArgConfig::LongOption);

    char const wifiConf[] = "/etc/wifi/wifi.conf";
}

int main(int argc, char **argv)
try
{
    ArgConfig &arg = ArgConfig::initialize("dhi:vVw:", 
            longOptions, longEnd, argc, argv);

    if (access(wifiConf, R_OK) == 0)
        arg.open(wifiConf);
    else
        wmsg << "Configuration file `" << wifiConf << "' not found" << endl;

    arg.versionHelp(usage, version, 1);

    Command command;
    command.execute(arg[0]);
    
}
catch (int x)
{
    return ArgConfig::instance().option("hv") ? 0 : x;
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cout << "Unexpected exception caught\n";
    return 1;
}
