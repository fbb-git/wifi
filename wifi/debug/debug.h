#ifndef INCLUDED_DEBUG_
#define INCLUDED_DEBUG_

#include <iosfwd>

class Debug
{
    bool d_debug;

    protected:
        Debug();
        std::string const &showDebug(std::string const &cmd) const;
};
        
#endif
