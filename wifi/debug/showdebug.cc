#include "debug.ih"

string const &Debug::showDebug(string const &cmd) const
{
    if (d_debug)
        cout << "Executing: " << cmd << '\n';
    return cmd;
}

