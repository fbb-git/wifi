#ifndef INCLUDED_SHOWDOTS_
#define INCLUDED_SHOWDOTS_

#include <thread>
#include <iosfwd>
#include <string>

class ShowDots
{
    static bool s_showDots;
    std::thread d_dots;

    public:
        ShowDots();
        ~ShowDots();

        void start(std::string const &label = "");
        void stop();

    private:

        static void showDots();
};

#endif
