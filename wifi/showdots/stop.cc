#include "showdots.ih"

void ShowDots::stop()
{
    s_showDots = false;

    if (d_dots.joinable())
        d_dots.join();

    cout << '\n';
}
