#include "showdots.ih"

void ShowDots::showDots()
{
    Selector selector;

    selector.setAlarm(0, 750000);

    while (s_showDots)
    {
        cout << '.' << flush;
        selector.wait();
    }
}
