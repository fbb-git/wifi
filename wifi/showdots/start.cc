#include "showdots.ih"

void ShowDots::start(string const &label)
{
    s_showDots = true;

    if (label.length())    
        cout << "\n" <<
             label << ' ';

    d_dots = thread(showDots);
}
