#ifndef INCLUDED_WIFI_
#define INCLUDED_WIFI_

#include <iosfwd>
#include <vector>

#include <bobcat/pattern>

#include "../ssid/ssid.h"
#include "../debug/debug.h"

class Wifi: private Debug
{
    std::vector<SSID> d_ssid;   // SSIDs hold the essential information about
                                // currently available SSIDs. Their values
                                // are determined by 'ssids()'
    SSID d_nextSSID;
    
    std::string d_wpa_conf;
    std::string d_interface;

    bool d_debug;

    static FBB::Pattern s_cellPattern;
    static FBB::Pattern s_quality;
    static FBB::Pattern s_encryptionKey;
    static FBB::Pattern s_iwconfigESSID;
    static FBB::Pattern s_inetAddr;
    static FBB::Pattern s_ESSID;
    static FBB::Pattern s_ssid;
    static FBB::Pattern s_WPA2;

    public:
        Wifi();

        void ssids();
        int select();
        void addNetwork(int idx);
        void restart() const;
        void up() const;
        void down() const;
        void showCurrentSSID() const;

    private:
        void handleLine(std::string const &line);
        void addSSID();
        void tryAdding();
        void removeKnownNetworks();
        void probe(std::string const &ssid);
        void ifupdown(char const *action) const;
        std::string getCurrentSSID() const;

//        static void showDots();
};
        
#endif
