#include "wifi.ih"

int Wifi::select()
{
    if (not d_ssid.size())
    {
        cout << "No access points detected. Maybe try again?\n";
//        return 0;
    }

    while (true)
    {
        cout << "Available:\n";
    
        for (size_t idx = 0; idx != d_ssid.size(); ++idx)
        {
            SSID const &ssid = d_ssid[idx];

            cout << "    " << (idx + 1) << ": " << ssid.ssid() << 
                    " (" << ssid.quality() << 
                    (ssid.type() == SSID::off ? ")" : ", P)") << '\n';
        }

        cout << 
            "Enter the number of the network you want to use "
                                                "(press `Enter' to quit): ";
        string line;
        getline(cin, line);

        size_t idx;
        istringstream ins(line);
        if (not (ins >> idx))
            return 0;

        if ((idx - 1) < d_ssid.size())
            return idx;

        cout << "\n"
                "Select a positive number not exceeding " << d_ssid.size() <<
                                            ". Try again...\n";
    }
}







