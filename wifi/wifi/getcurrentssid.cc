#include "wifi.ih"

string Wifi::getCurrentSSID() const
{
    Process iwconfig(Process::COUT, 
                        showDebug("/sbin/iwconfig " + d_interface));
    iwconfig.start();

    string line;

    string ret = 
            (getline(iwconfig, line) && (s_iwconfigESSID << line)) ?
                s_iwconfigESSID[1]
            :
                string();

    return ret;
}
