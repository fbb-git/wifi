#include "wifi.ih"

void Wifi::showCurrentSSID() const
{
    string ssid = getCurrentSSID();

    if (ssid.empty())
    {
        cout << "    Currently not connected to a wireless network\n";
        return;
    }

    cout << "    Connected to wireless network \"" << ssid << "\"\n";

    Selector selector;

    selector.setAlarm(0, 800'000);      // wait .8 sec

    for (size_t retry = 0; retry != 20; ++retry)
    {
        Process ifconfig(showDebug("/sbin/ifconfig " + d_interface));
        ifconfig.start();
    
        ifconfig.ignore(1000, '\n');    // skip the MAC info etc.
        string line;
        getline(ifconfig, line);
        if (s_inetAddr << line)
        {
            selector.noAlarm();
            if (retry != 0)
                cout.put('\n');
            cout << "    Our IP Address: " << s_inetAddr[1] << "\n\n";
            return;
        }
        try
        {
            cout .put('.').flush();
            selector.wait();
        }
        catch (...)
        {}
    }

    cout << "    Connected, but no IP address received: check "
                                                        "passwords/dhcp\n\n";
}
