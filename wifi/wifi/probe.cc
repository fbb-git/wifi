#include "wifi.ih"

void Wifi::probe(string const &existingSSID)
{
    // imsg << "Probing " << existingSSID << endl;

    while (true)
    {
                                // look for ssid in newly wifined SSIDs
        auto iter = find_if(d_ssid.begin(), d_ssid.end(), 
            [&](SSID const &newSSID)
            {
                return newSSID.ssid() == existingSSID;
            }
        );

        if (iter == d_ssid.end())   // not found: done
            return;

        d_ssid.erase(iter);        // remove the already available SSID
    }
}
