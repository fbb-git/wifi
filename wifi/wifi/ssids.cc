#include "wifi.ih"

void Wifi::ssids()
{
    ShowDots dots;

    dots.start("Scanning for networks");

    for (size_t wifi = 0; wifi != 10; ++wifi)
    {
        Process process(Process::IGNORE_CERR | Process::COUT, 
                            showDebug("/sbin/iwlist " + d_interface + 
                                        " scanning"));
        process.start();

        string line;
    
        while (getline(process, line))
            handleLine(line);
    
        addSSID();              // add the last one, not followed by `Cell'

        if (d_ssid.size() != 0)
            break;
    }

    dots.stop();

    removeKnownNetworks();      // remove networks already available in 
                                // wpa_supplicant.conf

                                // sort the available networks
    sort(d_ssid.begin(), d_ssid.end(), 
        [&](SSID const &first, SSID const &second)
        {
            return first.quality() > second.quality();
        }
    );
}




