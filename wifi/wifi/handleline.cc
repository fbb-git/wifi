#include "wifi.ih"

void Wifi::handleLine(string const &line)
{
    //cout << "NEXT: " << line << '\n';

    if (s_cellPattern << line)
        addSSID();
    else if (s_quality << line)
        d_nextSSID.quality(s_quality[1]);
    else if (s_encryptionKey << line)
        d_nextSSID.encryptionKey(s_encryptionKey[1]);
    else if (s_ESSID << line)
        d_nextSSID.essid(s_ESSID[1]);
    else if (s_WPA2 << line)
        d_nextSSID.setWPA2();
}
