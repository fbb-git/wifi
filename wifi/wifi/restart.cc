#include "wifi.ih"

void Wifi::restart() const
{
    down();
    sleep(1);
    up();
}
