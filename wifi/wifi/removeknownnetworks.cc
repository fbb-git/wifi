#include "wifi.ih"

void Wifi::removeKnownNetworks()
{
    ifstream networks(d_wpa_conf);

    if (!networks)
    {
        wmsg << "Cannot read " << d_wpa_conf << endl;
        return;
    }

    string line;
    while (getline(networks, line))
    {
        if (s_ssid << line)        // line contains ssid="..." specification
            probe(s_ssid[1]);
    }
}
