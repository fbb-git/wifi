#include "wifi.ih"

void Wifi::addNetwork(int idx)
{
    SSID &ssid = d_ssid[idx];

    if (ssid.needsKey())
        ssid.setKey();

    ssid.confirm();

    ofstream networks(d_wpa_conf, ios::app);

    if (!networks)
        fmsg << "Cannot append new network to " << d_wpa_conf << endl;

    networks << '\n' <<
                ssid << endl;
}
