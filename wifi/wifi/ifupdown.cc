#include "wifi.ih"

void Wifi::ifupdown(char const *action) const
{
    Process updown(showDebug(action + d_interface));
    updown.start();
    updown.waitForChild();
}



