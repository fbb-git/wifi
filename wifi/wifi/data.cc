#include "wifi.ih"

//          Cell 01 - Address: 00:26:F3:D5:A0:38
// Pattern Wifi::s_cellPattern("^\\s+Cell\\s");
Pattern Wifi::s_cellPattern(R"(\s+Cell\s)");

//                    Encryption key:on
// Pattern Wifi::s_encryptionKey("^\\s+Encryption key:(\\w+)");
Pattern Wifi::s_encryptionKey(R"(^\s+Encryption key:(\w+))");

//                    Quality=46/70  Signal level=-64 dBm
// Pattern Wifi::s_quality("^\\s+Quality=(\\d+)");
Pattern Wifi::s_quality(R"(^\s+Quality=(\d+))");

//                    ESSID:"Hamran"
//Pattern Wifi::s_ESSID("^\\s+E?SSID:\"([[:print:]]+)\"");
Pattern Wifi::s_ESSID(R"x(^\s+E?SSID:"([[:print:]]+)")x");

//                    ssid="styx"
//Pattern Wifi::s_ssid("^\\s+ssid=\"([[:print:]]+)\"");
Pattern Wifi::s_ssid(R"x(^\s+ssid="([[:print:]]+)")x");

//                    IE: IEEE 802.11i/WPA2
//Pattern Wifi::s_WPA2("^\\s+IE: .*WPA2");
Pattern Wifi::s_WPA2(R"(^\s+IE: .*WPA2)");

//                    iwconfig:     wlan0     IEEE 802.11abgn  ESSID:"styx" 
//Pattern Wifi::s_iwconfigESSID("ESSID:\"([[:print:]]+)\"");
Pattern Wifi::s_iwconfigESSID(R"x(ESSID:"([[:print:]]+)")x");

//                    inet 192.168.17.7  netmask 255.255.255.0 ...
//Pattern Wifi::s_inetAddr("^\\s+inet (\\S+)");
Pattern Wifi::s_inetAddr(R"(^\s+inet (\S+))");




