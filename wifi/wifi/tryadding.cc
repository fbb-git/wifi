#include "wifi.ih"

void Wifi::tryAdding()
{
    for (auto &ssid: d_ssid)
    {
        if (ssid.update(d_nextSSID))
            return;
    }

    d_ssid.push_back(d_nextSSID);
}
