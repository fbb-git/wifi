#include "wifi.ih"

void Wifi::up() const
{
    ifupdown("/sbin/ifup ");

    Selector selector;

    cout << "Waiting for wireless network ";
    selector.setAlarm(0, 800000);

    for (size_t step = 0; step != 40; ++step)
    {
        cout << '.' << flush;
        selector.wait();

        string ssid = getCurrentSSID();
        if (not ssid.empty())
        {
            selector.noAlarm();
            cout << '\n';
            showCurrentSSID();
            return;
        }
    }
    cout << "\n"
            "Failed to connect to a network. Maybe check the passwords?\n\n";
}






