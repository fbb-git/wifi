#include "wifi.ih"

Wifi::Wifi()
{
    ArgConfig &arg = ArgConfig::instance();
    arg.option(&d_wpa_conf, 'w');

    ArgConfig::instance().option(&d_interface, 'i');
    if (d_interface.empty())
        throw Exception() << "Wireless interface not specified.";

    if (arg.option('V'))
        cout << "Using wireless interface " << d_interface << '\n';
}
