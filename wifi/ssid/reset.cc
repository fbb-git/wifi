#include "ssid.ih"

void SSID::reset()
{
    d_ssid.clear();
    d_key.clear();
    d_type = unknown;
    d_quality = 0;
    d_newNetwork = false;
}
