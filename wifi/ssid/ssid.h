#ifndef INCLUDED_SSID_
#define INCLUDED_SSID_

#include <string>
#include <vector>

struct SSID
{
    friend std::ostream &operator<<(std::ostream &out, SSID const &ssid);

    enum Encryption
    {
        unknown,
        off,
        WEP,
        WPA2
    };

    enum Request
    {
        ERROR,
        REPLACE,
        INSERT,
        ACCEPT,
        REMOVE
    };

    private:
        std::string d_ssid;
        std::string d_key;
        Encryption d_type = unknown;
        size_t d_quality = 0;
        bool d_newNetwork = false;
        std::vector<std::string> d_config;

    public:
        SSID();

        void encryptionKey(std::string const &onOff);
        void quality(std::string const &value);
        void essid(std::string const &ssid);
        void setWPA2();
        bool update(SSID const &next);
        void setKey();

        void confirm();

        operator bool() const;
        bool needsKey() const;
        size_t quality() const;
        std::string const &ssid() const;
        std::string const &key() const;
        Encryption type() const;

        void reset();

    private:
        void fillConfig();
        void showConfig() const;
        Request request(std::vector<std::string>::iterator *pos, 
                        std::string *line);
        bool getRequest(int *idx, char *ch) const;
};

inline SSID::Encryption SSID::type() const
{
    return d_type;
}

inline size_t SSID::quality() const
{
    return d_quality;
}

inline std::string const &SSID::ssid() const
{
    return d_ssid;
}
        
inline std::string const &SSID::key() const
{
    return d_key;
}
        
inline bool SSID::needsKey() const
{
    return d_type != off;
}
        
#endif



