#include "ssid.ih"

void SSID::essid(std::string const &ssid)
{
    if (ssid.find("\\x") != string::npos)
        return;

    // cout << "ESSID: " << ssid << '\n';
    d_ssid = ssid;
}
