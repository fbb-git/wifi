#include "ssid.ih"

// called by scan/tryadding.cc

bool SSID::update(SSID const &next)
{
    if (d_ssid == next.d_ssid)
    {
        if (d_quality < next.d_quality)
            d_quality = next.d_quality;
        return true;
    }

    return false;
}
