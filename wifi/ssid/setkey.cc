#include "ssid.ih"

void SSID::setKey()
{
    string answer;
    do
    {
        cout << "Enter the key for network " << d_ssid << ": ";
        getline(cin, d_key);
    
        cout << "You entered `" << d_key << "', is this correct [y/n]? ";
        getline(cin, answer);
    }
    while (answer[0] != 'y');
}
