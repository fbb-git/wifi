#include "ssid.ih"

void SSID::fillConfig()
{
    d_config.push_back("network={");
    d_config.push_back("    id_str=\"dhcp\"");
    d_config.push_back("    ssid=\"" + ssid() + '"');

    switch (type())
    {
        case SSID::WEP:
            d_config.push_back("    key_mgmt=NONE");
            d_config.push_back("    wep_key0=" + key());
            d_config.push_back("    auth_alg=SHARED");
        break;

        case SSID::WPA2:
            d_config.push_back("    key_mgmt=WPA-PSK");
            d_config.push_back("    psk=\"" + key() + '"');
        break;

        default:
            d_config.push_back("    key_mgmt=NONE");
        break;
    }

    d_config.push_back("}");
}




