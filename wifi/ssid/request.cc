#include "ssid.ih"

SSID::Request SSID::request(vector<string>::iterator *pos, string *line)
{
    char ch;
    int idx;

    if (not getRequest(&idx, &ch))
        return ERROR;

    if (idx == 0)
        return ACCEPT;

    cout << "Enter line: ";
    getline(cin, *line);

    line->insert(0, 4, ' ');

    if (idx < 0)
    {
        *pos += -idx;
        return REMOVE;
    }

    *pos += idx;

    if (ch == '+' || ch == '-')
    {
        if (ch == '-')
            --*pos;
        return INSERT;
    }

    return REPLACE;
}






