#include "ssid.ih"

void SSID::confirm()
{
    fillConfig();

    while (true)
    {
        showConfig();

        cout << "\n"
            "Enter 0 to accept, 1.." << d_config.size() << "[+-] to\n"
            "                   edit (or insert before (-) or after (+))\n"
            "                   -nr: to delete that line number\n"
        "? ";

        auto pos = d_config.begin();
        string line;
        switch (request(&pos, &line))
        {
            case ERROR:
            continue;

            case REPLACE:
                *pos = line;
            break;            

            case INSERT:
                d_config.insert(pos, line);
            break;

            case REMOVE:
                d_config.erase(pos);
            break;

            case ACCEPT:
                cout << '\n';
            return;
        }
    }
}
