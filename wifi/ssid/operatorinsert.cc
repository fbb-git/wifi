#include "ssid.ih"

std::ostream &operator<<(std::ostream &out, SSID const &ssid)
{
    for (auto const &line: ssid.d_config)
        out << line << '\n';

    return out;
}


