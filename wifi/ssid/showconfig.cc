#include "ssid.ih"

void SSID::showConfig() const
{
    cout << " : " << d_config.front() << '\n';

    for (size_t lineNr = 1, end = d_config.size() - 1; 
            lineNr != end; 
                ++lineNr
    )
        cout << setw(2) << lineNr << ' ' << d_config[lineNr] << '\n';

    cout << " : " << d_config.back() << '\n';
}
