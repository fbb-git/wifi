#include "ssid.ih"

bool SSID::getRequest(int *idx, char *ch) const
{
    cin >> *idx;

    bool ret = true;

    if (cin && abs(*idx) < d_config.size()) // *ch not used with idx == 0
        *ch = cin.peek();
    else
    {
        cin.clear();
        cout << "?? Typo? Try again...\n";
        ret = false;
    }
    cin.ignore(1000, '\n');

    return ret;
}
