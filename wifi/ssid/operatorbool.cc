#include "ssid.ih"

SSID::operator bool() const
{
    bool ok = d_type != unknown && d_quality != 0 && not d_ssid.empty();

//    if (not ok)
//        cout << "Skipping: " << d_ssid << ", " << d_quality << ", " <<
//                                d_type << '\n';

    return ok;
}
