#include "command.ih"

void Command::execute(std::string const &cmd)
{
    (this->*findCmd(cmd))(); // execute the command matching 'cmd'
}
