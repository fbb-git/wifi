#include "command.ih"

void Command::eth0() const
{
    ArgConfig &arg = ArgConfig::instance();

    string request(arg[1]);

    string searchRE = R"(^eth0:\s+)" + request + R"(\s*$)";

    auto iter = arg.findRE(searchRE);

    if (iter == arg.end())
        throw Exception() << "`eth0 " << request << "' not implemented";

    ShowDots dots;
    dots.start();

    if (request == "on")
//        process("/sbin/ifup eth0");
        process("/sbin/ifconfig eth0 up");
    else if (request == "off")
//        process("/sbin/ifdown eth0");
        process("/sbin/ifconfig eth0 down");
    else
    {
//        process("/sbin/ifdown eth0");
        process("/sbin/ifconfig eth0 down");

        process("/sbin/ifscheme " + request);

        process("/sbin/ifup eth0");
    }

    dots.stop();
}
