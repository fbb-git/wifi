#include "command.ih"

void Command::process(string const &cmd) const
{
    Process proc(Process::IGNORE_COUT | Process::IGNORE_CERR, 
                    showDebug(cmd));

    proc.start();

    if (proc.waitForChild() != 0)
        throw Exception() << "Executing `" << cmd << "' failed";
}
