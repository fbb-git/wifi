#include "command.ih"

Command::Entry Command::s_action[] =
{
    Entry("add",    &Command::add),
    Entry("delete", &Command::del),
    Entry("on",     &Command::on),
    Entry("off",    &Command::off),
    Entry("status", &Command::status),
    Entry("search", &Command::search),
    Entry("eth0",   &Command::eth0),

    Entry("",       &Command::unknownCommand)
};
    
Command::Entry *Command::s_endAction = s_action + 
                                    sizeof(s_action) / sizeof(s_action[0]);
