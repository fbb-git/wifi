#include "command.ih"

void Command::search() const
{
    Wifi wifi;

    wifi.showCurrentSSID();
    wifi.ssids();

    if (int nr = wifi.select())
    {
        wifi.addNetwork(nr - 1);
        wifi.restart();
    }  
}
        
