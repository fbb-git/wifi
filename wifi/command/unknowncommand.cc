#include "command.ih"

void Command::unknownCommand() const
{
    throw Exception() << "Command `" << cmd() << "' not implemented";
}
        
