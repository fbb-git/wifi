#ifndef INCLUDED_COMMAND_
#define INCLUDED_COMMAND_

#include <string>
#include <bobcat/cmdfinder>
#include <bobcat/process>

#include "../debug/debug.h"

class Command: public FBB::CmdFinder<void (Command::*)() const>, private Debug
{
    static Entry s_action[];
    static Entry *s_endAction;

    public:
        Command();

        void execute(std::string const &cmd);   // execute a command

    private:
        void unknownCommand() const;
        void add() const;
        void search() const;
        void del() const;
        void on() const;
        void off() const;
        void status() const;
        void eth0() const;
        void process(std::string const &cmd) const;    
};

 
#endif

