#include "command.ih"

void Command::del() const
{
    status();

    NetworkDeleter deleter;
    deleter.optDeleteNetworks();
}
        
