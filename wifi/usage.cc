//                     usage.cc

#include "main.ih"

namespace 
{
    char const info[] = R"( [options] command
Where:
   [options] - optional arguments (short options between parentheses):
      --debug (-d)           - show the executed commands (before execution)
      --help (-h)            - provide this help
      --version (-v)         - show version information and terminate
      --interface (-i) iface - select a wireless interface (default: wlan0)
      --wpa-conf (-w) path   - wpa-supplicant conf. file to use (default:
                               /etc/wpa_supplicant/wpa_supplicant.conf)

   options can also be specified in /etc/wifi/wifi.conf

   command     - command to be executed. Implemented commands are:
       del     - shows all defined networks and optionally deletes some
       off     - explicitly stops the wireless interface
       on      - explicitly starts the wireless interface
       stat    - shows the currently connected wireless network.
                 (automatically taking down eth0)
       search - shows all available networks.
                 When selecting a new network it is added to the set of
                 known networks. New networks should provide DHCP.
                 When selecting an existing network it is given a high
                 priority, forcing its use.
                 Before adding a new interface to the wpa-supplicant conf
                 file it can be edited
                 After selecting a network the wireless interface restarts.
       eth0    - off, on, to take the interface up or down
                 north to connect to oostum.north
                 rc    to connect to the RC interface.

NOTE WELL: When problems connecting are experienced, hand-editing
     /etc/wpa_supplicant/wpa_supplicant.conf may be necessary.
)";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << author << "\n" <<
    progname << " V" << version << " " << years << "\n"
    "\n"
    "Usage: " << progname << info << '\n';
}




